module.exports = {
  title: 'VuePress Website',
  description: 'Test Template',
  base: '/vuepress/',
  dest: 'public',
  themeConfig: {
    docsDir: 'docs',
    search: false,
    nav: [
      { text: 'Home', link: '/' }
    ]
  }
}
